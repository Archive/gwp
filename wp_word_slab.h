/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __WordSlab_h_included__
#define __WordSlab_h_included__

#include <glib.h>
#include "wp_debug.h"
#include "wp_view.h"
#include "wp_slab.h"
#include "wp_object_types.h"
#include "wp_style.h"

class _WordSlab : public _Slab
{
  /* resizable array to hold the contents of this word slab*/
  char *word;
  /* used characters */
  unsigned int word_length;
  /* alloced characters */
  int word_alloced;

public:

  _WordSlab (WPStyle *set_style);
  _WordSlab (WPStyle *set_style, char *set_word, int set_word_length);

  virtual ObjectIDs classID () {return WordSlabID;}

  virtual Slab newOfThisType ();

  void setWord (char *set_word, int set_word_length);
  char *getWord ();
  int getWordLength ();

  virtual boolean insertChild (int i, Object what, GList *spots,
			       GList *views, int debug);
  virtual Object deleteChild (int i, GList *spots, GList *views, int debug,
			      boolean ignoreLostSpots);
  boolean replaceWord (char *new_word, int new_word_length,
		       GList *spots,
		       GList *views, int debug);
  boolean joinWithNext (GList *spots, GList *views, int debug);
  virtual boolean ensureBorder (int i, GList *spots,
				GList *views, int debug);
  virtual Slab createFollower (GList *spots, GList *views, int debug);
  virtual boolean childSizeChange (Slab child, GList *spots,
				   /* change in child */
				   int dLeft, int dUp, int dRight, int dDown,
				   GList *views, int debug);

  virtual Slab firstLowestContainer ();
  virtual Slab lastLowestContainer ();
  virtual int isFirst () {return 0;}
  virtual int isLast () {return 0;}

  virtual Slab getNextNode ();
  virtual Slab getNextNodeUp (Slab child, int depth);
  virtual Slab getNextNodeDown (int depth);
  virtual Slab getPrevNode ();
  virtual Slab getPrevNodeUp (Slab child, int depth);
  virtual Slab getPrevNodeDown (int depth);

  virtual Object getChildAt (int i);
  virtual void insertChildAt (Object what, int i);

  virtual int getIndexOfChild (Object what);
  virtual int getNumberOfChildren ();
  virtual void calculateSize (int debug);
  virtual WpPoint getPointOfChild (Slab child);
  virtual WpPoint getPointOfChild (int childNum);
  virtual Spot getSpotOfPoint (int px, int py);

  virtual void dumpToScreen (int tab);
  virtual void dd ();

  virtual void draw (int x, int y, GList *views, int debug);
  virtual void erase (int x, int y, GList *views);
};

#endif /* __WordSlab.h_included__ */
