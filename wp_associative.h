/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "wp_debug.h"


// a node class contains a name value pair, and may also contain a list of
// nodes. A node may be used as the base class for a more complex class.
class node
 {
  protected:
   friend class list_node;
   friend class leaf_node;
   // name of this node followed by value
   char *name_value;
   // parent node, remove ourselves if deleted
   node *parent;
   // next node in a list
   node *next;
   // list of our contents
   node *contents;
  public:
   node(const char *name="",const char *value="");
   virtual ~node();
   // name of this class
   virtual const char *class_name(void);
   
   // get the name contained in the name/value pair
   virtual const char *name_of(void);
   // get the value contained in the name/value pair
   virtual const char *value_of(void);
   // set name+value
   virtual int set_nv(const char *name,const char *value);
   
   // copy this node and its contents recursively.
   virtual node *copy(void);
   
   // tell our parent node to remove us.
   virtual int leave_parent(void);
   
   // list functions
   virtual node *find_node(const char *name);
   virtual const char *find_value(const char *name,const char *def=NULL);
   
   enum
    {
     at_end=0,
     at_start=1,
     allow_duplicate_names=2
    };
   
   virtual int add(node *node,int flags=at_start|allow_duplicate_names);
   // create a node and add it to the list
   virtual node *add(const char *name,const char *value,int flags=at_start|allow_duplicate_names);
   virtual int remove_node(node *node);
   virtual int destroy(const char *name);

   // child list traversal.
   virtual node *get_contents(void);
   virtual node *get_next(void);

   // print our tree
   virtual void dump(ostream &out,int depth=0);
 };

