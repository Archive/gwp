
typedef enum
{
  lt_none = 0,
  lt_list = 100,
  lt_list_item,
  lt_list_bullet_line,
  lt_list_bullet
} list_logical_type;


char *list_logical_type_to_string (list_logical_type a);
list_logical_type string_to_list_logical_type (unsigned const char *s);
Slab find_list_containing_slab (Slab slab, int *pos_ret);
int slab_is_list_content (Slab slab);
int slab_is_list_bullet (Slab slab);
int list_item_is_empty (Slab s);
Slab new_item_slab (WPStyleManager *sm, WPStyle *style,
		    int item_width, int debug);
int delete_list_item (WPDocument *doc, Spot spot);
Slab new_list_item_bullet (WPStyleManager *sm, WPStyle *style, int debug);
void insert_new_list_item (WPStyleManager *sm,
			   WPStyle *style, Spot spot,
			   int after_current);
void insert_new_list (WPDocument *doc, WPStyleManager *sm,
		      WPStyle *style, Spot spot);

GtkWidget *newListUI (WPView *view);
void deleteListUI (WPView */*view*/, GtkWidget *popup);
void showListUI (WPView *view, Spot spot, WPStyleManager *sm,
		 GtkWidget *popup, GdkEventButton *geb);
