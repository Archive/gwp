/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "wp_debug.h"


enum tag_id
 {
  TAG_A,TAG_ADDRESS,TAG_APPLET,TAG_AREA,TAG_B,TAG_BASEFONT,
  TAG_BIG,TAG_BLINK,TAG_BLOCKQUOTE,TAG_BODY,TAG_BR,
  TAG_CAPTION,TAG_CENTER,TAG_CITE,TAG_CODE,TAG_DD,
  TAG_DIR,TAG_DIV,TAG_DL,TAG_DT,TAG_EM,TAG_EMBED,
  TAG_FONT,TAG_FORM,TAG_FRAME,TAG_FRAMESET,TAG_H1,
  TAG_H2,TAG_H3,TAG_H4,TAG_H5,TAG_H6,TAG_HEAD,TAG_HR,
  TAG_HTML,TAG_I,TAG_ILAYER,TAG_IMG,TAG_INPUT,TAG_ISINDEX,
  TAG_KBD,TAG_KEYGEN,TAG_LAYER,TAG_LI,TAG_LINK,TAG_MAP,TAG_MENU,
  TAG_META,TAG_MULTICOL,TAG_NOBR,TAG_NOEMBED,TAG_NOFRAMES,
  TAG_NOLAYER,TAG_NOSCRIPT,TAG_OBJECT,TAG_OL,TAG_OPTION,
  TAG_P,TAG_PARAM,TAG_PLAINTEXT,TAG_PRE,TAG_S,TAG_SCRIPT,
  TAG_SELECT,TAG_TITLE
 };

// tag types
enum tag_types
 {
  tag_can_nest, // tags can be nested
  tag_no_close, // we do not expect this tag to be closed
  tag_cant_nest // can't nest tag
 };


class html_tag : public node
 {
   WPStyle *tag_style;
   html_tag *next;
   int reference_count;
   // how many nested tags we are within
   int depth;
  public:
   html_tag(WPStyle *initial_style=NULL);
   virtual ~html_tag();
   // add reference
   inline void reference(void) {reference_count++;}
   // remove reference
   inline static void dereference(html_tag *tag)
    {
     if (--tag->depth<=0)
      // no longer needed
      delete tag;
    }
   inline html_tag *next_tag(void)
    {return next;}
   // given a higher html_tag, and our own tag data, build a new style
   // if needed.
   void build_style (WPStyleManager *sm, html_tag *higher);
   // how deep are we?
   inline int get_depth(void) {return depth;}
   // get our style
   WPStyle *get_style(void) {return tag_style;}
 };
 
