
typedef enum
{
  wppt_roman,
  wppt_roman_2_col,
  wppt_roman_3_col,
  wppt_asian,
  wppt_arabic,
  wppt_scroll,
  wppt_default,
  wppt_unknown
} wp_page_type;


typedef struct
{
  int width;
  int height;
  int leftM;
  int upM;
  int rightM;
  int downM;
} page_size_info;


char *wp_page_type_to_string (wp_page_type wppt);
wp_page_type string_to_wp_page_type (unsigned const char *s);
void wp_page_setup_styles (wp_page_type wppt,
			   WPStyleManager *sm,
			   WPStyle **ret,
			   page_size_info *psi);
Slab wp_page_build (wp_page_type wppt,
		    WPStyleManager *sm,
		    page_size_info *psi);
GtkWidget *wp_new_page_setup_window (WPView *view, WPStyleManager *sm);

