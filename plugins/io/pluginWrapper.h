
#include "wp_debug.h"

// the old pluginWrapper class is now called pluginDocument

// class representing the capabilities of this particular plugin.
class pluginCap
 {
  protected:
  
  public:
   
   pluginCap(void);
   virtual ~pluginCap();
   
   // create new plugin document instance
   virtual class pluginDocument *newPluginDocument();
   virtual class pluginView *newPluginView();   
   
   virtual const char *getUIName(void);
   virtual const char *getFileExtension(void);
   virtual const char *getMimeType(void);
   virtual int canLoad(void);
   virtual int canNew(void);
   virtual int canSave(void);
 };

// a single instance view level
class pluginView
 {
  protected:
   
   // document plugin that this view refers to
   class pluginWrapper *docPlugin;

   // used when words are added
   WPStyle *current_insert_style;
   
  public:
   pluginView(void);
   virtual ~pluginView();

  virtual int replaceWord (WPView *view, Spot spot,
		   char *new_word, int new_word_length);
  virtual int deleteChild (WPView *view, Spot s);
  virtual int insertSlab (WPView *view, Spot spot, Slab slab);
  virtual Slab createFollowingWord (WPView *view, Spot spot);
  virtual int insertCharacter (WPView *view, Spot spot, unsigned long int c);
  virtual void buttonEvent (WPView *view, GdkEventButton *geb);
  virtual void showUI(WPView *view);
  virtual void hideUI(WPView *view);
  virtual void updateUI(WPView *view);

 };


// a single instance (document level)
class pluginDocument
{
 protected:
  
 public:
  pluginDocument (void);
  virtual ~pluginDocument ();

  virtual WPDocument *newDocument(WPDocument *doc);
  virtual WPDocument *loadDocument (char */*from_path*/,
				    WPDocument */*to_doc*/);
  virtual int saveDocument (WPDocument *doc, char *filename);
  
  int wrapper_id;
};


// a single instance 

//pluginWrapper *plugin_new (void);
pluginCap *pluginCapNew(void);
