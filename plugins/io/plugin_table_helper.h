
typedef enum
{
  lt_table = 200,
  lt_table_row,
  lt_table_cell
} table_logical_type;


char *table_logical_type_to_string (table_logical_type a);
table_logical_type string_to_table_logical_type (unsigned const char *s);
Slab find_table_containing_slab (Slab slab, int *pos_ret);
int slab_is_table_content (Slab slab);

Slab new_table_cell (WPStyleManager *sm, WPStyle *style);
Slab new_table_row (WPStyleManager *sm, WPStyle *style, int num_cells);
Slab new_table (WPStyleManager *sm, WPStyle *style,
		int num_cols, int num_rows);
void insert_new_table (WPDocument *doc, WPStyleManager *sm, WPStyle *style,
		       Spot spot, int num_cols, int num_rows);


GtkWidget *newTableUI (WPView *view);
void deleteTableUI (WPView *view, GtkWidget *popup);
void showTableUI (WPView */*view*/, GtkWidget *popup, GdkEventButton *geb);
