/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "wp_debug.h"


struct escapes
 {
  const char *name;
  int value;
 };

class escape_html
 {
   // this will probably need to be changed in the future for characters
   // over 255
   struct escapes *reverse_escapes[256];
  public:
   escape_html(void);
   ~escape_html();
   // get character code for the escaped character
   int get_char(const char *escape);
   // get escape code given a character code
   inline const char *get_escape(unsigned char code)
    {
     return reverse_escapes[code] ? reverse_escapes[code]->name : (const char *)NULL;
    }
 };

