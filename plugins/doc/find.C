#include <stdio.h>
#include <ctype.h>

#include <gmodule.h>

#include "wp_plugin.h"

#include "wp.h"
/*#include "wp_frame.h"*/
#include "wp_view.h"
#include "wp_document.h"

#include "wp_slab.h"
#include "wp_cont_slab.h"
#include "wp_word_slab.h"
#include "wp_lbt_slab.h"
#include "wp_st_slab.h"
#include "wp_image_slab.h"


typedef struct
{
  GtkWidget *dialog;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *hbox;
  WPView *view;
} find_ui_data;


static char *getUIName ()
{
  return "Find";
}


static find_ui_data *get_view_data (WPView *view)
{
  find_ui_data *fud;
  /*WPFrame *frame = wp_view_get_frame (view);*/
  /*if (!frame) return NULL;*/

  fud = (find_ui_data *) wp_view_get_plugin_scratch_space (view,
							   getUIName ());

  if (! fud)
    {
      fud = (find_ui_data *) calloc (1, sizeof (find_ui_data));
      wp_view_set_plugin_scratch_space (view, getUIName (), fud);
    }

  return fud;
}


int check_match (WPView *view, WPDocument */*doc*/,
		 Spot spot, int start, char *find_text)
{
  int find_text_len = strlen (find_text);
  char buf[ find_text_len + 5 ];
  int buf_len = 0;

  /* get find_text_len characters out of doc */

  buf[ 0 ] = '\0';
  Slab cur = spot->getSlab ();
  int pos = start;
  int get = 0;

  Slab end_slab = NULL;
  int end_position = 0;

  while (buf_len < find_text_len)
    {
      get = find_text_len - buf_len;
      char *word;
      int word_len;

      if (!cur) return 0;

      if (cur->classID () == WordSlabID)
	{
	  /* extract the char * from the wordslab */
	  WordSlab ws = (WordSlab) cur;
	  word = ws->getWord ();
	  word_len = ws->getWordLength ();
	}
      else if (cur->classID () == STSlabID)
	{
	  word = " ";
	  word_len = 1;
	}
      else
	{
	  cur = cur->getNextLeaf ();
	  pos = 0;
	  continue;
	}

      if (word_len - pos < get)
	get = word_len - pos;

      /* copy what we can */
      int i;
      for (i=0; i<get; i++)
	buf[ buf_len++ ] = word[ pos+i ];
      buf[ buf_len ] = '\0';

      end_slab = cur;
      end_position = pos+i;

      cur = cur->getNextLeaf ();
      pos = 0;
    }

  if (buf_len >= find_text_len &&
      strncasecmp (buf, find_text, find_text_len) == 0)
    {
      /* highlight the word being checked */
      wp_view_set_point (view, spot->getSlab (), start);
      if (end_slab)
	wp_view_set_mark (view, end_slab, end_position);
      else
	wp_view_set_mark (view, spot->getSlab (), start);
      wp_view_scroll_point_into_view (view);
      wp_view_hl_on (view);
      wp_view_refresh (view);

      return 1;
    }

  return 0;
}


static void search (find_ui_data *fud, char *find_text)
{
  if (!fud) return;
  if (strlen (find_text) == 0) return;

  WPView *view = WP_VIEW (mdi->active_view);
  WPDocument *doc = wp_view_get_document (view);
  if (!doc) return;

  Spot point = wp_view_get_point (view);
  Spot spot = doc->newDocumentSpot ();
  spot->setSlab (point->getSlab ());
  spot->setPosition (point->getPosition ());

  for (;;)
    {
      Slab cur = spot->getSlab ();
      int pos = spot->getPosition ();
      char *word;
      int word_len;

      if (!cur)
	{
	  /* hit the end of the document.  place the point
	     at the end so it will search from the start
	     if they hit find again */
	  Slab start = doc->getRoot ()->firstLowestContainer ();
	  wp_view_set_point (view, start, 0);
	  goto done;
	}
      if (cur->classID () == WordSlabID)
	{
	  /* extract the char * from the wordslab */
	  WordSlab ws = (WordSlab) cur;
	  word = ws->getWord ();
	  word_len = ws->getWordLength ();
	}
      else if (cur->classID () == STSlabID)
	{
	  word = " ";
	  word_len = 1;
	}
      else
	{
	  spot->setSlab (spot->getSlab ()->getNextLeaf ());
	  spot->setPosition (0);
	  continue;
	}

      /* look for the first character of find_text */
      int i;
      for (i=pos; i<word_len; i++)
	if (tolower (word[ i ]) == tolower (find_text[ 0 ]))
	  /* possible match */
	  if (check_match (view, doc, spot, i, find_text))
	    goto done;

      spot->setSlab (spot->getSlab ()->getNextLeaf ());
      spot->setPosition (0);
    }

 done:

  doc->deleteDocumentSpot (spot);
}


static void find_dialog_callback (GnomeDialog *dialog,
				  gint button_number, 
				  gpointer data)
{
  find_ui_data *fud = (find_ui_data *) data;
  if (!fud) return;
  /*WPFrame *frame = fud->frame;*/
  /*WPView *view = wp_frame_get_focus_view (frame);*/
  WPView *view = WP_VIEW (mdi->active_view);
  WPDocument *doc = wp_view_get_document (view);
  if (!doc) return;

  switch (button_number)
    {
    case 0: /* Find button */
      {
	char *find_text = gtk_entry_get_text (GTK_ENTRY (fud->entry));
	printf ("find '%s'\n", find_text);

	/* move forward so we don't match the same string again */
	if (! wp_view_move_to_next_character (view))
	  {
	    /* wrap to beginning of document */
	    Slab start = doc->getRoot ()->firstLowestContainer ();
	    wp_view_set_point (view, start, 0);
	  }

	search (fud, find_text);
        break;
      }
    case 1: /* Cancel Button */
      {
        gtk_widget_hide (GTK_WIDGET (dialog));
        break;
      }
    case 2: /* Help Button */
      break;
    }
}


static void do_find_callback (GtkWidget */*widget*/, gpointer /*client_data*/)
{
  /*WPView *view = wp_view_get_focus_view ((WPView *) client_data);*/
  WPView *view = WP_VIEW (mdi->active_view);
  WPDocument *doc = wp_view_get_document (view);
  if (!doc) return;

  printf ("in do_find_callback, view=%p\n", view);

  find_ui_data *fud = get_view_data (view);
  if (! fud->dialog)
    {
      fud->dialog = gnome_dialog_new ("Find", "Find", 
				      GNOME_STOCK_BUTTON_CANCEL, 
				      GNOME_STOCK_BUTTON_HELP,
				      NULL);
  
      fud->hbox = gtk_hbox_new (FALSE, 2);
      gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (fud->dialog)->vbox),
			 fud->hbox);
      gtk_widget_show (fud->hbox);

      fud->label = gtk_label_new ("String to find");
      gtk_container_add (GTK_CONTAINER (fud->hbox), fud->label);
      gtk_widget_show (fud->label);

      fud->entry = gtk_entry_new ();
      gtk_container_add (GTK_CONTAINER (fud->hbox), fud->entry);
      gtk_widget_show (fud->entry);

      gnome_dialog_set_default (GNOME_DIALOG (fud->dialog), 0);
      gtk_signal_connect (GTK_OBJECT (fud->dialog), "clicked",
			  GTK_SIGNAL_FUNC (find_dialog_callback), fud);
    }

  gtk_widget_show (fud->dialog);
}


static void newUI (WPView *view)
{
  /* insert a menu item under the Tools menu */

  if (!view) return;

  find_ui_data *fud = get_view_data (view);
  if (!fud) return;

  if (fud->view) return; /* already done */
  fud->view = view;

  GnomeUIInfo entry[2];
  entry[0].type = GNOME_APP_UI_ITEM;
  entry[0].label = getUIName ();
  entry[0].hint = NULL;
  entry[0].moreinfo = do_find_callback;
  entry[0].user_data = view;
  entry[0].unused_data = NULL;
  entry[0].pixmap_type = GNOME_APP_PIXMAP_STOCK;
  entry[0].pixmap_info = GNOME_STOCK_MENU_SEARCH;
  entry[0].accelerator_key = 0;
  entry[1].type = GNOME_APP_UI_ENDOFINFO;

  GnomeApp *app = gnome_mdi_get_app_from_view (GTK_WIDGET (view));

  int menu_pos;
  char menu_item_path[ strlen (entry[0].label) + 20 ];
  sprintf (menu_item_path, "Edit/%s", entry[0].label);
  if (gnome_app_find_menu_pos (app->menubar, menu_item_path, &menu_pos))
    return; /* already done */

  gnome_app_insert_menus_with_data (app, "Edit/", entry, view);
}


static void deleteUI (WPView */*view*/)
{
}


static void hideUI (WPView */*view*/)
{
}


static void showUI (WPView */*view*/)
{
}


extern "C"
{
  int wp_plugin_fill_vtable (PluginVTable *pvt);
  char *g_module_check_init (GModule *module);
}


int wp_plugin_fill_vtable (PluginVTable *pvt)
{
  pvt->get_ui_name = getUIName;
  pvt->new_ui = newUI;
  pvt->delete_ui = deleteUI;
  pvt->show_ui = showUI;
  pvt->hide_ui = hideUI;
  return 0;
}


char *g_module_check_init (GModule */*module*/)
{
  printf ("find -- g_module_check_init called\n");
  return NULL;
}
