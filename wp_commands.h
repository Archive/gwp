/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _wp_commands_h
#define _wp_commands_h

#include <gtk/gtk.h>
#include "wp_debug.h"

extern void file_new_view_cmd_callback (GtkWidget *widget,
					gpointer client_data);
extern void file_new_doc_cmd_callback (GtkWidget *widget,
				       gpointer client_data);
extern void file_new_doc_type_cmd_callback (GtkWidget */*widget*/,
					    gpointer client_data);
extern void file_new_doc_ok_cmd_callback (GtkWidget */*widget*/,
					  gpointer client_data);
extern void file_new_doc_cancel_cmd_callback (GtkWidget */*widget*/,
					      gpointer client_data);
extern void file_pagesetup_cmd_callback (GtkWidget *widget,
					 gpointer client_data);
extern void file_printpreview_cmd_callback (GtkWidget *widget,
					    gpointer client_data);
extern void file_print_cmd_callback (GtkWidget *widget,
				     gpointer client_data);
extern void file_close_cmd_callback(GtkWidget *widget,
				    gpointer   client_data);
extern void file_quit_cmd_callback(GtkWidget *widget,
				   gpointer   client_data);

extern void edit_undo_cmd_callback(GtkWidget *widget,
				   gpointer   client_data);
extern void edit_redo_cmd_callback(GtkWidget *widget,
				   gpointer   client_data);
extern void edit_cut_cmd_callback(GtkWidget *widget,
				  gpointer   client_data);
extern void edit_copy_cmd_callback(GtkWidget *widget,
				   gpointer   client_data);
extern void edit_paste_cmd_callback(GtkWidget *widget,
				    gpointer   client_data);
extern void edit_selectall_cmd_callback(GtkWidget *widget,
					gpointer   client_data);
extern void edit_clear_cmd_callback(GtkWidget *widget,
				    gpointer   client_data);

extern void view_showrulers_cmd_callback(GtkWidget *widget,
					 gpointer   client_data);

extern void view_refresh_cmd_callback(GtkWidget *widget,
				      gpointer   client_data);

extern void plugins_refresh_cmd_callback(GtkWidget *widget,
					 gpointer   client_data);

/*
extern void document_switch_cmd_callback(GtkWidget *widget,
					 gpointer   client_data);
*/

extern void help_about_cmd_callback(GtkWidget *widget,
				    gpointer   client_data);
extern void help_license_cmd_callback(GtkWidget *widget,
				      gpointer   client_data);

extern void dump_doc_structure_callback(GtkWidget */*widget*/,
					gpointer  client_data);

#endif /* _wp_commands_h */
