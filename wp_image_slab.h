/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ImageSlab_h_included__
#define __ImageSlab_h_included__

#include <string.h>
#include <gnome.h>
#include <gdk/gdk.h>
#include "wp_slab.h"
#include "wp_object_types.h"
#include "wp_debug.h"


class _ImageSlab : public _Slab
{
  GdkImlibImage *img;
  GdkPixmap *pix;
  GdkWindow *window;
  char *image_file;

public:

  _ImageSlab (WPStyle *set_style,
	      GdkImlibImage *set_img, char *set_image_file);
  ~_ImageSlab ();

  virtual ObjectIDs classID () {return ImageSlabID;}

  char *get_image_file () {return image_file;}
  void set_image_file (char *im_file) {image_file = strdup (im_file);}
  unsigned char *get_rgb_data () {return img->rgb_data;}
  unsigned int get_rgb_width () {return img->rgb_width;}
  unsigned int get_rgb_height () {return img->rgb_height;}

  Slab newOfThisType ();
  GdkPixmap *getPixmap ();
  void calculateSize (int debug);
  void draw (int x, int y, GList *views, int debug);
  void erase (int x, int y, GList *views);
  boolean insertChild (int i, Object what, GList *spots,
		       GList *views, int debug);
  Object deleteChild (int i, GList *spots, GList *views,
		      int debug, boolean ignore_lost_spots);
  boolean childSizeChange (Slab child, GList *spots,
			   int dLeft, int dUp, int dRight, int dDown,
			   GList *views, int debug);
  boolean ensureBorder (int i, GList *spots, GList *views, int debug);

  int getIndexOfChild (Object what);

  Slab getChildAt (int i);

  void insertChildAt (Object what, int i);

  void dumpToScreen (int tab);

  virtual void dd ();

  int isLast ();

  int isFirst ();

  Slab firstLowestContainer ();
  Slab lastLowestContainer ();

  Slab getNextNode ();
  Slab getNextNodeUp (Slab child, int depth);
  Slab getNextNodeDown (int depth);

  Slab getPrevNode ();
  Slab getPrevNodeUp (Slab child, int depth);
  Slab getPrevNodeDown (int depth);

  virtual int getNumberOfChildren ();

  WpPoint getPointOfChild (Slab child);
  WpPoint getPointOfChild (int childNum);

  Spot getSpotOfPoint (int px, int py);
};

#endif /* __WordSlab.h_included__ */
