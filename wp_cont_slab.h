/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ContSlab_h_included__
#define __ContSlab_h_included__

#include <glib.h>
#include "wp_slab.h"
#include "wp_object_types.h"
#include "wp_debug.h"


#include <libgnomeprint/gnome-print.h>

typedef void (*draw_ps_callback) (WPDocument *doc,
				  GnomePrintContext *pc,
				  ContSlab slab,
				  int x, int y);
typedef void (*draw_x_callback) (WPView *g, ContSlab slab, int x, int y);
typedef char *(*draw_ascii_callback) (WPDocument *doc, ContSlab slab,
				      int x, int y);


/* a position along with a size */
typedef struct
{
  int x, y;
  int left, up, right, down;
} slab_extent;


class _ContSlab : public _Slab
{
  draw_ps_callback psdrawfunc;
  draw_x_callback xdrawfunc;
  draw_ascii_callback asciidrawfunc;


  void slideArea (GList *views, int x0, int y0, int x1, int y1,
		  int dx, int dy, int debug);
  void slide3Area (GList *views,
		   slab_extent a_orig, slab_extent a_new,
		   slab_extent b_orig, slab_extent b_new,
		   slab_extent c_orig, slab_extent c_new,
		   int debug);

protected:
  GList *children;



public:

  boolean attemptToStealChildren (GList *spots, GList *views, int debug);

  _ContSlab (WPStyle *set_style,
	     draw_ps_callback set_psdrawfunc,
	     draw_x_callback set_xdrawfunc,
	     draw_ascii_callback set_asciidrawfunc);
  virtual Slab newOfThisType ();

  virtual ObjectIDs classID () {return ContSlabID;}

  virtual int getDirection ();

  virtual boolean insertChild (int i, Object what, GList *spots,
			       GList *views, int debug);
  virtual Object deleteChild (int i, GList *spots, GList *views,
			      int debug, boolean ignoreLostSpots);
  virtual boolean ensureBorder (int i, GList *spots,
				GList *views, int debug);
  virtual Slab createFollower (GList *spots, GList *views, int debug);


  int csc_will_fit (Slab child,
		    int dLeft, int dUp, int dRight, int dDown,
		    int *child_stays,
		    int debug);
  slab_extent csc_cover_children (Slab c0, Slab c1, int inc_c0, int inc_c1);

  boolean csc_this_size_might_change (GList *spots, GList *views, int debug);
  slab_extent csc_rect_relative_to_absolute (slab_extent in);
  boolean csc_boot_children (GList *spots, int numFit,
			     GList *views, int debug);
  boolean csc_steal_children (GList *spots, int numFit,
			      GList *views, int debug);
  boolean csc_boot_some (Slab child, GList *spots,
			 int dLeft, int dUp, int dRight, int dDown,
			 int num_fit,
			 GList *views, int debug);
  boolean csc_steal_some (Slab child, GList *spots,
			  int dLeft, int dUp, int dRight, int dDown,
			  int fit,
			  GList *views, int debug);
  boolean csc_boot_child (Slab child, GList *spots,
			  int dLeft, int dUp, int dRight, int dDown,
			  int fit,
			  GList *views, int debug);


  virtual boolean childSizeChange (Slab child, GList *spots,
				   /* change in child */
				   int dLeft, int dUp, int dRight, int dDown,
				   GList *views, int debug);

  virtual Slab firstLowestContainer ();
  virtual Slab lastLowestContainer ();

  virtual Slab getNextNode ();
  virtual Slab getNextNodeUp (Slab child, int depth);
  virtual Slab getNextNodeDown (int depth);
  virtual Slab getPrevNode ();
  virtual Slab getPrevNodeUp (Slab child, int depth);
  virtual Slab getPrevNodeDown (int depth);

  virtual GList *getChildren () {return children;}
  virtual Object getChildAt (int i);
  virtual void insertChildAt (Object what, int i);

  virtual int getIndexOfChild (Object what);
  virtual int getNumberOfChildren ();
  virtual void calculateSize (int debug);
  virtual WpPoint getPointOfChild (Slab child);
  virtual WpPoint getPointOfChild (int childNum);
  virtual Spot getSpotOfPoint (int px, int py);

  virtual void dumpToScreen (int tab);
  virtual void dd ();

  virtual int isFirst ();
  virtual int isLast ();

  int find_free_space ();
  int alignmentAdd (int *x, int *y);

  virtual void draw_margin (int x, int y, GList *views, int debug);
  virtual void draw (int x, int y, GList *views, int debug);
  virtual void erase (int x, int y, GList *views);
  virtual char *drawASCII (WPDocument *doc, int x, int y);
  virtual void drawPS (WPDocument *doc, GnomePrintContext *pc, int x, int y);

  virtual int styleChangeNotify (WPStyle *s, GList *spots,
				 GList *views, int debug);
};

#endif /* __ContSlab.h_included__ */
