/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __wpobjecttypes_h_included
#define __wpobjecttypes_h_included

#include "wp_debug.h"

class _Object;
typedef _Object *Object;

class _Spot;
typedef _Spot *Spot;

class _ContSlab;
typedef _ContSlab *ContSlab;

class _PageSlab;
typedef _PageSlab *PageSlab;

class _LBTSlab;
typedef _LBTSlab *LBTSlab;

class _WordSlab;
typedef _WordSlab *WordSlab;

class _STSlab;
typedef _STSlab *STSlab;

class _ImageSlab;
typedef _ImageSlab *ImageSlab;

class _DrawnSlab;
typedef _DrawnSlab *DrawnSlab;

class _Slab;
typedef _Slab *Slab;

class _Character;
typedef _Character *Character;

class _Spot;
typedef _Spot *Spot;

typedef enum
{
  ObjectID,
  SpotID,
  ContSlabID,
  PageSlabID,
  LBTSlabID,
  WordSlabID,
  STSlabID,
  ImageSlabID,
  DrawnSlabID,
  SlabID,
  CharacterID
} ObjectIDs;


#endif /* __wpobjecttypes_h_included */
