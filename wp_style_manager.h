/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __WPStyleManager_h__
#define __WPStyleManager_h__

class WPStyleManager;

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include "wp_view.h"
#include "wp_style.h"
#include "wp_debug.h"

typedef struct
{
  char *name;
  GList *styles;
} style_group;


class WPStyleManager
{
  GList *style_groups;

public:
  WPStyleManager ();
  ~WPStyleManager();

  style_group *WPStyleManager::newStyleGroup (char *name);
  style_group *getStyleGroupByName (char *name);

  WPStyle *newStyle (char *group_name, char *style_name);
  WPStyle *newStyle (style_group *sg, char *style_name);
  WPStyle *copyStyle (char *group_name, char *style_name,WPStyle *style);
  WPStyle *getStyleByName (char *group_name, char *style_name);
  WPStyle *getStyleByName (style_group *sg, char *style_name);

  /* free what this returns */
  char *unique_style_name (style_group *sg);

  WPStyle *findOrCreateStyle (style_group *sg,
			      /*char *font_name,*/ /* font name */
			      char *font_family,
			      GnomeFontWeight font_weight,
			      gboolean font_italic,
			      double font_points,
			      GdkColor *fg, /* foreground */
			      GdkColor *bg, /* background */
			      WPAlignment algn, /* alignment */
			      WPDirection dir, /* direction */
			      WPSizePolicy xsp, /* x size policy */
			      WPSizePolicy ysp, /* x size policy */
			      unsigned long int fl, /* fixed left */
			      unsigned long int fu, /* fixed up */
			      unsigned long int fr, /* fixed right */
			      unsigned long int fd, /* fixed down */
			      unsigned long int fml, /* fixed margin left */
			      unsigned long int fmu, /* fixed margin up */
			      unsigned long int fmr, /* fixed margin right */
			      unsigned long int fmd, /* fixed margin down */
			      unsigned long int mc, /* max children */
			      WPMarginRenderingPolicy mrp, /*mar. rend. */
			      WPWhenEmptyPolicy we,
			      int underline); /* delete when empty? */

  WPStyle *createStyle (style_group *sg,
			/*char *font_name,*/ /* font name */
			char *font_family,
			GnomeFontWeight font_weight,
			gboolean font_italic,
			double font_points,
			GdkColor *fg, /* foreground */
			GdkColor *bg, /* background */
			WPAlignment algn, /* alignment */
			WPDirection dir, /* direction */
			WPSizePolicy xsp, /* x size policy */
			WPSizePolicy ysp, /* x size policy */
			unsigned long int fl, /* fixed left */
			unsigned long int fu, /* fixed up */
			unsigned long int fr, /* fixed right */
			unsigned long int fd, /* fixed down */
			unsigned long int fml, /* fixed margin left */
			unsigned long int fmu, /* fixed margin up */
			unsigned long int fmr, /* fixed margin right */
			unsigned long int fmd, /* fixed margin down */
			unsigned long int mc, /* max children */
			WPMarginRenderingPolicy mrp, /* margin rendering */
			WPWhenEmptyPolicy we,
			int underline); /* delete when empty? */
};

int colorcmp (GdkColor *a, GdkColor *b);

#endif /*__WPStyleManager_h__*/
