%define  ver     0.3.2
%define  rel     1
%define  prefix  /usr/local

Summary: a gnome word processor
Name: gwp
Version: %ver
Release: %rel
Copyright: GPL
Group: Applications/Wordprocessors
Source: ftp://ftp.hungry.com/pub/hungry/gwp/gwp-%{ver}.tar.gz
Url:http://www.hungry.com/products/gwp/
BuildRoot:/var/tmp/gwp-root
#Docdir: %{prefix}/doc

Requires: gnome-libs >= 0.30
#Requires: gnome-xml
#Requires: gnome-print

%description
GNOME based word processor

%prep
%setup

%build
./configure --prefix=%prefix

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
#rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT



%changelog

%files
#%defattr(-, root, root)

%doc AUTHORS ChangeLog NEWS README COPYING TODO

%{prefix}/share/locale/*/LC_MESSAGES/gwp.mo
%{prefix}/lib/gwp/plugins/*
%{prefix}/share/gnome/help/Gwp/C/*
%{prefix}/libexec/go/plugins/*
%{prefix}/bin/gwp
%{prefix}/share/apps/Applications/Gwp.desktop
