/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _WPPlugin_h
#define _WPPlugin_h


#include <gnome.h>
#include <gmodule.h>
/*#include "wp_document.h"*/
/*#include "wp_frame.h"*/
#include "wp_view.h"
#include "wp_debug.h"

#include "wp_spot.h"
class WPDocument;


typedef char *(*PluginGetFileExtension) ();
typedef char *(*PluginGetMimeType) ();
typedef char *(*PluginGetUIName) ();

typedef int (*PluginCanLoad) ();
typedef int (*PluginCanSave) ();
typedef int (*PluginCanNew) ();

typedef WPDocument *(*PluginLoadDocument) (char *filename);
typedef int (*PluginInsertDocument) (char *filename, WPView *view);
typedef int (*PluginSaveDocument) (WPDocument *doc, char *filename);
typedef WPDocument *(*PluginNewDocument) (char *title);

typedef int (*PluginDeleteChild) (WPView *view, Spot s);
typedef int (*PluginInsertCharacter) (WPView *view, Spot spot,
				      unsigned long int c);
typedef int (*PluginReplaceWord) (WPView *view, Spot spot,
				  char *new_word, int new_word_length);
typedef Slab (*PluginCreateFollowingWord) (WPView *view, Spot spot);
typedef int (*PluginInsertSlab) (WPView *view, Spot spot, Slab slab);

typedef void (*PluginButtonEvent) (WPView *view, GdkEventButton *geb);

typedef void (*PluginNewUI) (WPView *view);
typedef void (*PluginDeleteUI) (WPView *view);
typedef void (*PluginShowUI) (WPView *view);
typedef void (*PluginHideUI) (WPView *view);
typedef void (*PluginUpdateUI) (WPView *view);

typedef void (*PluginSetAutosave) (WPView *view, gboolean onoff);


typedef struct
{
  PluginGetFileExtension get_file_extension;
  PluginGetMimeType get_mime_type;
  PluginGetUIName get_ui_name;

  PluginCanLoad can_load;
  PluginCanLoad can_insert;
  PluginCanSave can_save;
  PluginCanNew can_new;

  PluginLoadDocument load_document;
  PluginInsertDocument insert_document;
  PluginSaveDocument save_document;
  PluginNewDocument new_document;

  PluginDeleteChild delete_child;
  PluginInsertCharacter insert_character;
  PluginReplaceWord replace_word;
  PluginCreateFollowingWord create_following_word;
  PluginInsertSlab insert_slab;

  PluginButtonEvent button_event;

  PluginNewUI new_ui;
  PluginDeleteUI delete_ui;
  PluginShowUI show_ui;
  PluginHideUI hide_ui;
  PluginUpdateUI update_ui;

  PluginSetAutosave set_autosave;
} PluginVTable;


typedef int (*PluginFillVTable)(PluginVTable *pvt);



class WPPlugin
{
  PluginVTable *pvt;

public:
  WPPlugin (PluginVTable *set_pvt);
  ~WPPlugin ();

  char *getFileExtension ();
  char *getMimeType ();
  char *getUIName ();

  int canLoad ();
  int canInsert ();
  int canSave ();
  int canNew ();

  WPDocument *loadDocument (char *filename);
  int insertDocument (char *filename, WPView *view);
  int saveDocument (WPDocument *doc, char *filename);
  WPDocument *newDocument (char *title);

  int deleteChild (WPView *view, Spot s);
  int insertCharacter (WPView *view, Spot spot, unsigned long int c);
  int replaceWord (WPView *view, Spot spot,
		   char *new_word, int new_word_length);
  Slab createFollowingWord (WPView *view, Spot spot);
  int insertSlab (WPView *view, Spot spot, Slab slab);

  void buttonEvent (WPView *view, GdkEventButton *geb);

  void newUI (WPView *view); /* create the widgets */
  void deleteUI (WPView *view); /* delete the widgets */
  void showUI (WPView *view); /* show the widgets */
  void hideUI (WPView *view); /* hide the widgets */
  void updateUI (WPView *view); /* change settings based on cursor posistion */

  void setAutoSave (WPView *view, gboolean onoff); /* turn autosave on/off */
};

#endif /* _WPPlugin_h */
