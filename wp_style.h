/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __WPStyle_h__
#define __WPStyle_h__

#include <gnome.h>
#include <libgnomeprint/gnome-font.h>

class WPStyle;

#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "wp_debug.h"


typedef enum
{
  ALIGN_LOW = 0,
  ALIGN_HIGH,
  ALIGN_CENTER
} WPAlignment;


typedef enum
{
  DIR_RIGHT = 0,
  DIR_LEFT,
  DIR_UP,
  DIR_DOWN
} WPDirection;


typedef enum
{
  SIZE_POLICY_BY_CHILDREN = 0,
  SIZE_POLICY_FIXED,
  SIZE_POLICY_BY_PARENT
} WPSizePolicy;


typedef enum
{
  MARGIN_RENDERING_POLICY_BLANK = 0,
  MARGIN_RENDERING_POLICY_FILL,
  MARGIN_RENDERING_POLICY_OUTLINE
} WPMarginRenderingPolicy;


typedef enum
{
  WHEN_EMPTY_REMOVE = 0,
  WHEN_EMPTY_KEEP
} WPWhenEmptyPolicy;


char *style_alignment_to_string (WPAlignment a);
WPAlignment style_string_to_alignment (unsigned const char *s);
char *style_direction_to_string (WPDirection a);
WPDirection style_string_to_direction (unsigned const char *s);
char *style_size_policy_to_string (WPSizePolicy a);
WPSizePolicy style_string_to_size_policy (unsigned const char *s);
char *style_margin_rendering_policy_to_string (WPMarginRenderingPolicy a);
WPMarginRenderingPolicy
 style_string_to_margin_rendering_policy (unsigned const char *s);
char *style_when_empty_policy_to_string (WPWhenEmptyPolicy a);
WPWhenEmptyPolicy style_string_to_when_empty_policy (unsigned const char *s);




class WPStyle
{
  /*
  char *font_name;
  GHashTable *scaled_fonts;
  */
  char *font_family;
  GnomeFontWeight font_weight;
  gboolean font_italic;
  double font_points;

  GdkColor *fg;
  GdkColor *bg;
  char *name;
  WPAlignment alignment;
  WPDirection direction;
  WPSizePolicy xsp;
  WPSizePolicy ysp;

  unsigned long int fixed_left;
  unsigned long int fixed_up;
  unsigned long int fixed_right;
  unsigned long int fixed_down;

  unsigned long int margin_left;
  unsigned long int margin_up;
  unsigned long int margin_right;
  unsigned long int margin_down;

  WPMarginRenderingPolicy margin_rendering;
  WPWhenEmptyPolicy when_empty;

  int max_children;

  int underline;

  /*GdkFont *load_font (double scale);*/

public:
  WPStyle (char *name);
  ~WPStyle();
  // Use copyStyle in wp_style_manager!!
  void copy(WPStyle *old);

  char *getName () {return name;}

  GdkFont *getFont (double scale);

  /*
  char *getFontName ();
  void setFontName (char *set_font_name);
  */

  char *getFontFamily ();
  void setFontFamily (char *set_font_name);
  GnomeFontWeight getFontWeight ();
  void setFontWeight (GnomeFontWeight set_w);
  gboolean getFontItalic ();
  void setFontItalic (gboolean set_fi);
  double getFontPoints ();
  void setFontPoints (double set_fp);

  void setForeground (GdkColor *fg);
  GdkColor* getForeground ();

  void setBackground (GdkColor *bg);
  GdkColor* getBackground ();

  void setAlignment (WPAlignment set_a);
  WPAlignment getAlignment ();

  void setDirection (WPDirection set_d) {direction = set_d;} /* XXX check it */
  WPDirection getDirection () {return direction;}

  void setXSizePolicy (WPSizePolicy set_sp) {xsp = set_sp;}
  WPSizePolicy getXSizePolicy () {return xsp;}

  void setYSizePolicy (WPSizePolicy set_sp) {ysp = set_sp;}
  WPSizePolicy getYSizePolicy () {return ysp;}

  void setFixedLeft (unsigned long int v);
  void setFixedUp (unsigned long int v);
  void setFixedRight (unsigned long int v);
  void setFixedDown (unsigned long int v);
  unsigned long int getFixedLeft ();
  unsigned long int getFixedUp ();
  unsigned long int getFixedRight ();
  unsigned long int getFixedDown ();

  void setMarginLeft (unsigned long int v);
  void setMarginUp (unsigned long int v);
  void setMarginRight (unsigned long int v);
  void setMarginDown (unsigned long int v);
  unsigned long int getMarginLeft ();
  unsigned long int getMarginUp ();
  unsigned long int getMarginRight ();
  unsigned long int getMarginDown ();

  void setMarginRendering (WPMarginRenderingPolicy set_mrp);
  WPMarginRenderingPolicy getMarginRendering ();

  void setWhenEmpty (WPWhenEmptyPolicy set_we);
  WPWhenEmptyPolicy getWhenEmpty ();

  void setMaxChildren (unsigned long int m);
  unsigned long int getMaxChildren ();

  int screen_string_width (char *text, int length, double scale);
  int printer_string_width (char *text, int length, double scale);
  int font_ascent (double scale);
  int font_descent (double scale);

  void setUnderline (int u);
  int getUnderline ();
};


#endif /*__WPStyle_h__*/
