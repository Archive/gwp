
#include "wp_plugin.h"


WPPlugin::WPPlugin (PluginVTable *set_pvt)
{
  pvt = set_pvt;
}


WPPlugin::~WPPlugin ()
{
}


char *WPPlugin::getFileExtension ()
{
  if (pvt->get_file_extension)
    return (*(pvt->get_file_extension)) ();
  return NULL;
}


char *WPPlugin::getMimeType ()
{
  if (pvt->get_mime_type)
    return (*(pvt->get_mime_type)) ();
  return NULL;
}


char *WPPlugin::getUIName ()
{
  if (pvt->get_ui_name)
    return (*(pvt->get_ui_name)) ();
  return NULL;
}


int WPPlugin::canLoad ()
{
  if (pvt->can_load)
    return (*(pvt->can_load)) ();
  return 0;
}


int WPPlugin::canInsert ()
{
  if (pvt->can_insert)
    return (*(pvt->can_insert)) ();
  return 0;
}


int WPPlugin::canSave ()
{
  if (pvt->can_save)
    return (*(pvt->can_save)) ();
  return 0;
}


int WPPlugin::canNew ()
{
  if (pvt->can_new)
    return (*(pvt->can_new)) ();
  return 0;
}


int WPPlugin::deleteChild (WPView *view, Spot spot)
{
  if (pvt->delete_child)
    return (*(pvt->delete_child)) (view, spot);
  return 0;
}


int WPPlugin::insertCharacter (WPView *view,
			       Spot spot,
			       unsigned long int c)
{
  if (pvt->insert_character)
    return (*(pvt->insert_character)) (view, spot, c);
  return 0;
}


int WPPlugin::insertSlab (WPView *view, Spot spot, Slab slab)
{
  if (pvt->insert_slab)
    return (*(pvt->insert_slab)) (view, spot, slab);
  return 0;
}


int WPPlugin::replaceWord (WPView *view, Spot spot,
			   char *new_word, int new_word_length)
{
  if (pvt->replace_word)
    return (*(pvt->replace_word)) (view, spot, new_word, new_word_length);
  return 0;
}


Slab WPPlugin::createFollowingWord (WPView *view, Spot spot)
{
  if (pvt->create_following_word)
    return (*(pvt->create_following_word)) (view, spot);
  return NULL;
}


void WPPlugin::buttonEvent (WPView *view, GdkEventButton *geb)
{
  if (pvt->button_event)
    (*(pvt->button_event)) (view, geb);
}


WPDocument *WPPlugin::loadDocument (char *filename)
{
  if (pvt->load_document)
    {
      WPDocument *doc = (*(pvt->load_document)) (filename);
      if (!doc) return NULL;
      doc->setPlugin (this);
      return doc;
    }
  return NULL;
}


int WPPlugin::insertDocument (char *filename, WPView *view)
{
  if (pvt->insert_document)
    return (*(pvt->insert_document)) (filename, view);
  return -1;
}


WPDocument *WPPlugin::newDocument (char *title)
{
  if (pvt->new_document)
    {
      WPDocument *doc = (*(pvt->new_document)) (title);
      if (!doc) return NULL;
      doc->setPlugin (this);
      return doc;
    }
  return NULL;
}


int WPPlugin::saveDocument (WPDocument *doc, char *filename)
{
  if (pvt->save_document)
    return (*(pvt->save_document)) (doc, filename);
  return 0;
}


void WPPlugin::newUI (WPView *view)
{
  if (pvt->new_ui)
    (*(pvt->new_ui)) (view);
}


void WPPlugin::deleteUI (WPView *view)
{
  if (pvt->delete_ui)
    (*(pvt->delete_ui)) (view);
}


void WPPlugin::showUI (WPView *view)
{
  if (pvt->show_ui)
    (*(pvt->show_ui)) (view);
}


void WPPlugin::hideUI (WPView *view)
{
  if (pvt->hide_ui)
    (*(pvt->hide_ui)) (view);
}


void WPPlugin::updateUI (WPView *view)
{
  if (pvt->update_ui)
    (*(pvt->update_ui)) (view);
}


void WPPlugin::setAutoSave (WPView *view, gboolean onoff)
{
  if (pvt->set_autosave)
    (*(pvt->set_autosave)) (view, onoff);
}
