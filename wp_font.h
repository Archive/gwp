/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997-1999 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _wp_font_h
#define _wp_font_h

#include "wp_print.h"
#include "wp_debug.h"


typedef struct GnomeDisplayFont
{
  GnomeFontUnsized *unsized_font; /* font before scaling */
  GnomeFont *gnome_font; /* scaled font */
  double scale; /* scaling factor requested by a view */

  char *x_font_name; /* x name that got us gdk_font */
  GdkFont *gdk_font; /* used for drawing */
};


char *gnome_font_weight_to_string (GnomeFontWeight gfw);
GnomeFontWeight string_to_gnome_font_weight (char *weight);
GnomeDisplayFont *gnome_get_display_font (char *family,
					  GnomeFontWeight weight,
					  gboolean italic,
					  double points,
					  double scale);

/****************************/
/* these munge x font names */
/****************************/

char *getFontComponent (char *font_name, unsigned int pos);
char *setFontComponent (char *font_name, unsigned int pos, char *s);

char *getFontFoundry (char *font_name);
char *getFontFamily (char *font_name);
char *getFontWeight (char *font_name);
GnomeFontWeight getGnomeFontWeight (char *font_name);
char *getFontSlant (char *font_name);
gboolean fontIsItalic (char *font_name);
char *getFontSWidth (char *font_name);
char *getFontAdStyle (char *font_name);
int getFontPixelSize (char *font_name);
int getFontPointSize (char *font_name);
int getFontResolutionX (char *font_name);
int getFontResolutionY (char *font_name);
char *getFontSpace (char *font_name);
int getFontAverageWidth (char *font_name);
char *getFontRegistry (char *font_name);
char *getFontEncoding (char *font_name);


/* these malloc, so free what they return */

char *setFontFoundry (char *font_name, char *s);
char *setFontFamily (char *font_name, char *s);
char *setFontWeight (char *font_name, char *s);
char *setFontSlant (char *font_name, char *s);
char *setFontSWidth (char *font_name, char *s);
char *setFontAdStyle (char *font_name, char *s);
char *setFontPixelSize (char *font_name, int i);
char *setFontPointSize (char *font_name, int i);
char *setFontResolutionX (char *font_name, int i);
char *setFontResolutionY (char *font_name, int i);
char *setFontSpace (char *font_name, char *s);
char *setFontAverageWidth (char *font_name, int i);
char *setFontRegistry (char *font_name, char *s);
char *setFontEncoding (char *font_name, char *s);

#endif /* _wp_font_h */
