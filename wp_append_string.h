/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "wp_debug.h"


#define APNDSIZE 100

class append_string
 {
   char buff[APNDSIZE],*ptr;
   int len,max_len;
   void expand(void)
    {
     int want_len=max_len*2;
     if (ptr==buff)
      {
       char *new_ptr=(char *)malloc(want_len);
       memcpy(new_ptr,ptr,len+1);
       ptr=new_ptr;
      }
     else
      ptr=(char *)realloc(ptr,want_len);
     max_len=want_len;
    }
  public:
   append_string(void)
    {
     ptr=buff;
     *ptr=0;
     len=0;
     max_len=APNDSIZE;
    }
   ~append_string()
    {
     if (ptr!=buff)
      free(ptr);
    }
   inline void operator+=(char c)
    {
     if (len>(max_len-2))
      expand();
     ptr[len]=c;
     ptr[len+1]=0;
     len++;
    }
   inline void operator+=(const char *s)
    {
     int need=strlen(s)+len;
     while(need>(max_len-2))
      expand();
     strcpy(ptr+len,s);
     len=need;
    }
   inline void operator=(const char *s)
    {
     int need=strlen(s);
     while(need>(max_len-2))
      expand();
     strcpy(ptr,s);
     len=strlen(ptr);
    }
   operator char *() const
    {
     return ptr;
    }
 };
