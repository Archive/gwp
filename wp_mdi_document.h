/*
 * Copyright � 1999 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "wp_document.h"

#ifndef __WP_MDI_DOCUMENT_H__
#define __WP_MDI_DOCUMENT_H__

/* gtk style wrapper around WPDocument */

#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include <gnome.h>

BEGIN_GNOME_DECLS

#define WP_MDI_DOCUMENT(obj)          GTK_CHECK_CAST (obj, wp_mdi_document_get_type (), WPMDIDocument)
#define WP_MDI_DOCUMENT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, wp_mdi_document_get_type (), WPMDIDocumentClass)
#define IS_WP_MDI_DOCUMENT(obj)       GTK_CHECK_TYPE (obj, wp_mdi_document_get_type ())

typedef struct _WPMDIDocument       WPMDIDocument;
typedef struct _WPMDIDocumentClass  WPMDIDocumentClass;


struct _WPMDIDocument
{
  GnomeMDIChild mdi_child;
  WPDocument *doc;
};


struct _WPMDIDocumentClass
{
  GnomeMDIChildClass parent_class;
};

WPMDIDocument *wp_mdi_document_new (WPPlugin *plugin, char *title);
WPMDIDocument *wp_mdi_document_load (WPPlugin *plugin, char *filename);
WPDocument *wp_mdi_document_get_document (WPMDIDocument *mdi_doc);
guint wp_mdi_document_get_type ();

END_GNOME_DECLS

#endif /* __WP_MDI_DOCUMENT_H__ */
