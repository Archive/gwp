/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>


class WPPreferences
{
  GtkWidget *properties_dialog;
  GtkWidget *notebook;

  void update_views ();

 public:
  /* backups */
  gboolean b_draw_fast;
  GdkColor *b_margin_color;
  GdkColor *b_background_color;
  GdkColor *b_foreground_color;
  GdkColor *b_cursor_color;
  GnomeMDIMode b_mdi_mode;
  gboolean b_show_rulers;
  gboolean b_debug;
  gboolean b_allow_all_fonts;
  GtkMetricType b_metric;

  /* active values */
  gboolean draw_fast;
  GdkColor *margin_color;
  GdkColor *background_color;
  GdkColor *foreground_color;
  GdkColor *cursor_color;
  GnomeMDIMode mdi_mode;
  gboolean show_rulers;
  gboolean debug;
  gboolean allow_all_fonts;
  GtkMetricType metric;

  WPPreferences ();
  ~WPPreferences ();

  void showDialog ();
  void backup ();
  void apply ();
};

extern WPPreferences *preferences;

extern void preferences_cmd_callback (GtkWidget */*widget*/,
				      gpointer /*client_data*/);
