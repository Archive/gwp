/*
 * Copyright � 1997-1999 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __WPDocument_h__
#define __WPDocument_h__

class WPDocument;

#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "wp_slab.h"
#include "wp_spot.h"
#include "wp_style_manager.h"
#include "wp_debug.h"


class WPPlugin;
typedef struct _WPMDIDocument WPMDIDocument;

class WPDocument
{
  Slab root; /* the root of the document tree */
  GList *spots; /* various cursors (point, mark, etc) */
  /*GList *views;*/ /* list of views showing this document */

  int debug; /* set to 0 for much spew, set to -1 for less. */
  char *document_name;

  /* the controlling plugin for this document */
  WPPlugin *plugin;

  WPMDIDocument *mdi_doc;

  int read_only;
  int display_refresh;
  GHashTable *plugin_scratchspace;

public:
  WPDocument (char *doc_name);
  ~WPDocument ();

  void set_mdi_document (WPMDIDocument *mdi_doc);
  WPMDIDocument *get_mdi_document ();

  void setPlugin (WPPlugin *set_plugin);
  WPPlugin *getPlugin () {return plugin;}
  void *get_plugin_scratch_space (char *plugin_name);
  void set_plugin_scratch_space (char *plugin_name, void *space);

  void dumpToScreen ();

  void paint_slab (WPView *g, Slab top, GdkRectangle *a);
  void erase_slab (WPView *g, Slab top, GdkRectangle *a);
  void refresh ();

  GList *get_document_views ();
  /*
  void addView (WPView *g);
  void deleteView (WPView *g);
  */

  Spot newDocumentSpot ();
  void deleteDocumentSpot (Spot dead_spot);

  int styleChangeNotify (WPStyle *s);

  int setWordStyle (WPStyleManager *sm, Spot spot, char *style_name);
  int setLineStyle (WPStyleManager *sm, Spot spot, char *style_name);
  int setColumnStyle (WPStyleManager *sm, Spot spot, char *style_name);
  int setPageStyle (WPStyleManager *sm, Spot spot, char *style_name);
  int setDocumentStyle (WPStyleManager *sm, Spot spot, char *style_name);
  int setStyle (WPStyleManager *sm, Spot spot,
		char *group_name, char *style_name);
  int setStyles (WPStyleManager *sm,
		 WPStyle *document_style,
		 WPStyle *page_style,
		 WPStyle *column_style,
		 WPStyle *line_style,
		 WPStyle *word_style);

  Slab getRoot ();
  void setRoot (Slab s) {root = s;}

  void setReadOnly(int flag);
  int getReadOnly();

  void displayRefreshOn ();
  void displayRefreshOff ();
  int displayRefreshIsOn ();
 
  char *getDocumentName ();
  void setDocumentName (char *new_name);
  unsigned long int getDocumentLeft ();
  unsigned long int getDocumentUp ();
  unsigned long int getDocumentRight ();
  unsigned long int getDocumentDown ();
  unsigned long int getDocumentWidth ();
  unsigned long int getDocumentHeight ();

  int deleteChild (Spot spot);
  int replaceWord (Spot spot, char *new_word, int new_word_length);
  Slab createFollowingWord (Spot spot, WPStyle *style);
  int insertCharacter (Spot spot, unsigned long int c,
		       WPStyle *style);
  int insertSlab (Spot spot, Slab ns);
  int ensureBorder (Spot spot);

  /* cursor motion -- these return 1 if the cursor moved, 0 otherwise */

  int moveToPreviousCharacter (Spot spot);
  int moveToNextCharacter (Spot spot);
  int moveToPreviousWord (Spot spot);
  int moveToNextWord (Spot spot);
  int moveToStartOfLine (Spot spot);
  int moveToEndOfLine (Spot spot);
  int moveToNextLine (Spot spot);
  int moveToPreviousLine (Spot spot);
  int moveToNextLine (Spot spot, int preferred_x_pos, int preferred_y_pos);
  int moveToPreviousLine (Spot spot, int preferred_x_pos, int preferred_y_pos);

  void setSpotToPoint (Spot spot, WPView *view, int x, int y);
  void getPointFromSpot (Spot spot, WPView *view, int *x, int *y, int *height);

  int sanityCheckSpots ();

  int spot_to_index (Spot spot);
  void set_spot_to_index (Spot spot, int index);

  static GHashTable *documents;
  static guint32 untitled_document_count;
};

/*extern WPDocument *wp_create_document (WPView *g);*/

char *wp_get_text_between_spots (Spot start, Spot end);

int wp_slab_is_between (Slab a, Slab b, Slab c);
int wp_spot_order (Spot a, Spot b);
int wp_slab_order (Slab a, Slab b);


#endif /* __WPDocument_h__ */
