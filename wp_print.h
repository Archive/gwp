#ifndef __WP_PRINT_H_INCLUDED__
#define __WP_PRINT_H_INCLUDED__

#include <gtk/gtk.h>
#include <gnome.h>

#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-printer-dialog.h>

#include "wp_document.h"

void gwp_print (GnomePrinter *printer, WPDocument *doc);


#endif /* __WP_PRINT_H_INCLUDED__ */
