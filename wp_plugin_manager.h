/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _WPPluginManager_h
#define _WPPluginManager_h


#include "gnome.h"
/*#include "libgnome/gnome-dl.h"*/
#include "wp_document.h"
/*#include "wp_frame.h"*/
#include "wp_debug.h"


class WPPluginManager
{
  void scan_for_plugins_in_directory (char *dir);

public:
  WPPluginManager();
  ~WPPluginManager();

  static WPPluginManager *thePluginManager ();
  void scan_for_plugins ();

  static GList *plugins;
  static GHashTable *plugins_hash;

  WPPlugin *get_plugin_by_name (char *name);
};


#endif /* _WPPluginmanager_h */
