/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Character_h_included__
#define __Character_h_included__

#include "wp_object.h"
#include "wp_debug.h"


class _Character : public _Object
{
  unsigned long int c;

public:

  _Character (unsigned long int c) {this->c = c;}

  virtual ObjectIDs classID () {return CharacterID;}
  unsigned long int charValue (){return c;}
};

#endif /* __Character.h_included__ */
