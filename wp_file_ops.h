/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _wp_fileops_h
#define _wp_fileops_h

#include "wp_debug.h"


extern void file_open_cmd_callback (GtkWidget *widget,
				    gpointer client_data);
extern void file_insert_cmd_callback (GtkWidget *widget,
				      gpointer client_data);
extern void file_save_cmd_callback (GtkWidget *widget,
				    gpointer client_data);
extern void file_saveas_cmd_callback (GtkWidget *widget,
				      gpointer client_data);

extern void file_load_by_extension_callback (GtkWidget *widget,
					     gpointer client_data);
extern void file_load_by_plugin_callback (GtkWidget *w,
					  gpointer client_data);

extern void file_insert_by_extension_callback (GtkWidget *widget,
					       gpointer client_data);
extern void file_insert_by_plugin_callback (GtkWidget *w,
					    gpointer client_data);

extern void file_save_by_extension_callback (GtkWidget *widget,
					     gpointer client_data);
extern void file_save_by_plugin_callback (GtkWidget *w,
					  gpointer client_data);


#endif /* _wp_fileops_h */
