/* BLAH BLAH!  BLAHBLAHBLAH!
 * Copyright � 1998 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __WP_VIEW_H__
#define __WP_VIEW_H__

#include <stddef.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkhbox.h>
#include <gnome.h>
#include "plugin.h"

BEGIN_GNOME_DECLS

#define WP_VIEW(obj)          GTK_CHECK_CAST ((obj), wp_view_get_type (), WPView)
#define WP_VIEW_CLASS(klass)  GTK_CHECK_CLASS_CAST ((klass), wp_view_get_type (), WPViewClass)
#define IS_WP_VIEW(obj)       GTK_CHECK_TYPE ((obj), wp_view_get_type ())

typedef struct _WPView       WPView;
typedef struct _WPViewClass  WPViewClass;

END_GNOME_DECLS

#include "wp_debug.h"
#include "wp_style.h"
#include "wp_document.h"
/*typedef struct _WPFrame WPFrame;*/
/*#include "wp_frame.h"*/ /* XXX */

BEGIN_GNOME_DECLS

struct _WPView
{
  GtkHBox hbox;
  GtkWidget *vbox;
  GtkWidget *plugin_toolbar;
  GtkWidget *drawing_area;
  GdkPixmap *back_pixmap;

  GtkObject *vadj;
  GtkObject *hadj;

  GtkWidget *hscrollbar;
  GtkWidget *vscrollbar;

  GtkWidget *hruler;
  GtkWidget *vruler;

  GdkGC *gc;

  double scale;

  Spot point;
  Spot mark;

  /*
  GdkColor *fg;
  GdkColor *bg;
  GdkColor *cursor_color;
  */

  gint hl_on;
  gint have_selection;

  GdkPixmap *point_save_under;
  GdkPixmap *mark_save_under;

  int user_set_cursor_pos_set;
  int user_set_cursor_pos_x;
  int user_set_cursor_pos_y;

  GHashTable *plugin_scratchspace;

  gboolean next_key_is_command;
  gboolean cursor_on;

  gboolean mouse_1_down;
};


struct _WPViewClass
{
  GtkHBoxClass parent_class;

  /*
  void (* close)  (WPView *);
  void (* minimize) (WPView *);
  void (* maximize)  (WPView *);
  */
};


guint wp_view_get_type (void);
GtkWidget *wp_view_new ();

/*WPFrame *wp_view_get_frame (WPView *view);*/
/*WPView *wp_view_get_focus_view (WPView *view);*/

/*gint view_drawing_area_event (GtkWidget *widget,
                                GdkEvent *event, void *viewp);*/

GtkWidget *wp_view_get_plugin_toolbar (WPView *view);

void *wp_view_get_plugin_scratch_space (WPView *view, char *plugin_name);
void wp_view_set_plugin_scratch_space (WPView *view,
				       char *plugin_name, void *space);

// Made this global so that gwp_plugin_program_register() can call
// it the first time ... ugly hack, I know ... /Joachim
void wp_view_add_to_plugins_menu (WPView *view, plugin_info *info);


/*GtkWidget *wp_view_get_basewidget(WPView *view);*/

void wp_view_destroy(WPView* view);

/*void wp_view_show(WPView *view);*/
GtkWidget *wp_view_get_drawing_area (WPView *wpv);

void wp_view_set_color (WPView *view, GdkColor *c);
GdkColor *wp_view_get_cursor_color (WPView *view);

/*GdkColormap *wp_view_get_colormap(WPView *view);*/


void wp_view_draw_arc (WPView *view,
		       int filled,
		       long int x1, long int y1,
		       long int x2, long int y2,
		       int angle1, int angle2);
void wp_view_draw_line (WPView *view,
			long int x1,
			long int y1,
			long int x2,
			long int y2);
void wp_view_draw_string (WPView *view,
			  char *word, int len,
			  GdkFont *font,
			  long int x, long int y,
			  WPDirection d);
void wp_view_refresh(WPView *view);
void wp_view_fill_rect (WPView *view,
			long int x, long int y,
			long int width, long int height);
void wp_view_draw_rect (WPView *view,
			long int x, long int y,
			long int width, long int height);
void wp_view_copy_area (WPView *view, 
			int sx, int sy,
			int width, int height,
			int dx, int dy);
void wp_view_copy_from_pixmap (WPView *view,
			       GdkPixmap *pix,
			       int sx, int sy,
			       int width, int height,
			       int dx, int dy);

int wp_view_get_scroll_x (WPView *view);
int wp_view_get_scroll_y (WPView *view);
int wp_view_get_x (WPView *view);
int wp_view_get_y (WPView *view);
int wp_view_get_width (WPView *view);
int wp_view_get_height (WPView *view);

int wp_view_get_pixel_width (WPView *view);
int wp_view_get_pixel_height (WPView *view);

double wp_view_get_scale_factor (WPView *view);
void wp_view_set_scale_factor (WPView *view, double set_scale);

void wp_view_dumpToScreen (WPView *view);

WPDocument *wp_view_get_document (WPView *view);

void wp_view_draw_cursor(WPView *view);
void wp_view_erase_cursor(WPView *view);

void wp_view_delete_range (WPView *view);
void wp_view_delete_char(WPView *view);
void wp_view_insert_char(WPView *view, unsigned long int c);

void wp_view_move_to_start_of_line (WPView *view);
void wp_view_move_to_end_of_line (WPView *view);
int wp_view_move_to_previous_character (WPView *view);
int wp_view_move_to_next_character (WPView *view);
int wp_view_move_to_previous_word (WPView *view);
int wp_view_move_to_next_word (WPView *view);
void wp_view_move_to_next_line (WPView *view);
void wp_view_move_to_previous_line (WPView *view);


/* these keep the cursor in the same general X position
   during consecutive prev/next line commands. */

void wp_view_set_user_set_cursor_pos (WPView *view);
void wp_view_clear_user_set_cursor_pos (WPView *view);
int wp_view_user_set_cursor_pos_is_set (WPView *view);
void wp_view_get_user_set_cursor_pos (WPView *view, int *x, int *y);

void wp_view_preferences_changed (WPView *view);

void wp_view_sync_rulers (WPView* view);
void wp_view_sync_scrollbars (WPView* view);
void wp_view_scroll_point_into_view (WPView *view);

void wp_view_hl_on (WPView *view);
void wp_view_hl_off (WPView *view);
int wp_view_hl_is_on (WPView *view);

Spot wp_view_get_point (WPView *view);
Spot wp_view_get_mark (WPView *view);

/* These seem better to use ... /Joachim
 *void wp_view_set_point (WPView *view, Spot point);
 *void wp_view_set_mark (WPView *view, Spot mark);
 */
void wp_view_set_point (WPView *view, Slab slab, int position);
void wp_view_set_mark (WPView *view, Slab slab, int position);

void wp_view_clear_back_pixmap (WPView *view);
void wp_view_copy_back_pixmap (WPView *view);

void wp_view_cursor_on (WPView *view);
void wp_view_cursor_off (WPView *view);

void wp_view_set_autosave (WPView *view, gboolean onoff);

END_GNOME_DECLS

#endif /* __WP_VIEW_H__ */
