/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Slab_h_included__
#define __Slab_h_included__

#include <stdio.h>
#include <stddef.h>
#include <glib.h>
#include "wp_object.h"
#include "wp_view.h"
#include "wp_style.h"
#include "wp_spot.h"
#include "wp_utils.h"
#include "wp_debug.h"

class _Slab;
typedef _Slab *Slab;


class _Slab : public _Object
{
protected:

  int left, up, right, down;
  /*WPSizePolicy xSizePolicy, ySizePolicy;*/
  Slab parent;
  WPStyle *style;
  void *plugin_data;

public:

  _Slab (WPStyle *set_style);
  virtual ~_Slab ();

  void setStyle (WPStyle *set_style, GList *spots, GList *views, int debug);
  WPStyle *getStyle ();

  void setPluginData (void *d) {plugin_data = d;}
  void *getPluginData () {return plugin_data;}

  virtual ObjectIDs classID () {return SlabID;}

  virtual Slab newOfThisType () {return (Slab) NULL;}

  virtual void dumpToScreen (int /*tab*/){}
  virtual void dd () {printf ("Slab?\n");}

  virtual void draw (int /* x */, int /* y */,
		     GList */*views*/, int /* debug */){}
  virtual void erase (int /* x */, int /* y */,
		      GList */*views*/){}

  virtual boolean insertChild (int /* i */, Object /* what */,
			       GList */* spots */,
			       GList */*views*/,
			       int /* debug */){return false;}
  virtual Object deleteChild (int /* i */, GList */* spots */,
			      GList */*views*/,
			      int /* debug */,
			      boolean /* ignore_lost_spots */)
    {return (_Slab *) NULL;}
  virtual boolean ensureBorder (int /* i */, GList */* spots */,
				GList */*views*/,
				int /* debug */){return false;}

  virtual boolean childSizeChange (Slab /* child */, GList */* spots */,
				   int /* dLeft */, int /* dUp */,
				   int /* dRight */, int /* dDown */,
				   GList */*views*/,
				   int /* debug */){return false;}

  virtual boolean trim_slab (GList *spots, GList *views, int debug);

  virtual void setParent (Slab parent);
  virtual Slab getParent ();
  virtual int getNumberOfChildren (){return -1;}
  virtual Object getChildAt (int /* i */){return (_Slab *) NULL;}
  virtual void insertChildAt (Object /* what */, int /* i */){}
  virtual int getIndexOfChild (Object /* what */){return -1;}
  virtual int slabIsInside (Slab s);

  virtual void calculateSize (int /* debug */){}
  virtual WpPoint getPointOfChild (Slab /* child */){return newWpPoint(0,0);}
  virtual WpPoint getPointOfChild (int /* child_num */)
    {return newWpPoint(0,0);}
  virtual Spot getSpotOfPoint (WpPoint p);
  virtual Spot getSpotOfPoint (int /* px */, int /* py */)
    {return (Spot) NULL;}


  virtual int getLeft ();
  virtual int getUp ();
  virtual int getRight ();
  virtual int getDown ();
  virtual int getWidth ();
  virtual int getHeight ();
  virtual void setLeft (int l);
  virtual void setUp (int u);
  virtual void setRight (int r);
  virtual void setDown (int d);

  virtual int getMLeft ();
  virtual int getMUp ();
  virtual int getMRight ();
  virtual int getMDown ();
  virtual int getMWidth ();
  virtual int getMHeight ();
  virtual void setMLeft (int l);
  virtual void setMUp (int u);
  virtual void setMRight (int r);
  virtual void setMDown (int d);

  /* returns 1 if anything changed. */
  virtual int styleChangeNotify (WPStyle *s, GList *spots,
				 GList *views, int debug);


  virtual WPSizePolicy getMajorSizePolicy ();
  virtual int getMajorLow (int direction);
  virtual int getMajorHigh (int direction);
  virtual int getMMajorLow (int direction);
  virtual int getMMajorHigh (int direction);

  /*virtual void setXSizePolicy (WPSizePolicy xsp);*/
  virtual WPSizePolicy getXSizePolicy ();
  /*virtual void setYSizePolicy (WPSizePolicy ysp);*/
  virtual WPSizePolicy getYSizePolicy ();

  virtual int isFirst ();
  virtual int isLast ();
  virtual int getDepth ();
  virtual Slab firstLowestContainer (){return (_Slab *) NULL;}
  virtual Slab lastLowestContainer (){return (_Slab *) NULL;}
  virtual Slab topOfTree ();

  virtual Slab getPrevLeaf ();
  virtual Slab getNextLeaf ();

  virtual Slab getNextNode (){return (_Slab *) NULL;}
  virtual Slab getNextNodeUp (Slab /* child */, int /* depth */)
    {return (_Slab *) NULL;}
  virtual Slab getNextNodeDown (int /* depth */){return (_Slab *) NULL;}
  virtual Slab getPrevNode (){return (_Slab *) NULL;}
  virtual Slab getPrevNodeUp (Slab /* child */, int /* depth */)
    {return (_Slab *) NULL;}
  virtual Slab getPrevNodeDown (int /* depth */){return (_Slab *) NULL;}

  int slab_overlaps_view (int x, int y, WPView *g);
};

#endif /* __Slab.h_included__ */
