/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Spot_h_included__
#define __Spot_h_included__

#include "wp_object.h"
#include "wp_object_types.h"
#include "wp_debug.h"

class _Spot : public _Object
{
  Slab slab;
  int position;

public:

  _Spot ();
  _Spot (Slab slab, int position);

  virtual ObjectIDs classID () {return SpotID;}

  /*
  Spot rise ();
  Spot fall ();
  */
  void rise ();
  void fall ();
  void sink ();
  void hopBack ();
  void hopForward ();
  void hopBackLeaf ();
  void hopForwardLeaf ();
  Slab getSlab ();
  void setSlab (Slab s);
  int getPosition ();
  void setPosition (int p);
};

#endif /* __Spot.h_included__ */
