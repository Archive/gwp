#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GWP"

(test -f $srcdir/configure.in \
  && test -f $srcdir/INSTALL \
  && test -d $srcdir/plugins) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gwp directory"
    exit 1
}

. $srcdir/macros/autogen.sh
