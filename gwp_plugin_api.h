/* gwp_plugin_api.h
 * Copyright (C) 1998 Chris Lahey
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GWP_PLUGIN_API_H__
#define __GWP_PLUGIN_API_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "plugin.h"
  
  int gwp_plugin_document_create (gint context, gchar *title);
  int gwp_plugin_document_open (gint context, gchar *title);
  gboolean gwp_plugin_document_close (gint docid);
  void gwp_plugin_text_append (gint docid, gchar *buffer, gint length);
  void gwp_plugin_text_insert (gint docid, gchar *buffer, gint length,
			       gint position);
  void gwp_plugin_document_show (gint docid);
  int gwp_plugin_document_current (gint context);
  char *gwp_plugin_document_filename (gint docid);
  char *gwp_plugin_text_get (gint docid);
  char *gwp_plugin_text_get_selected_text (gint docid);
// Changed the return type, previously gchar *, to void due to
// a freshly stolen plugin.C from GO. Thanks Chris! :) /Joachim
  void gwp_plugin_text_set_selected_text (gint docid,
					  gchar *data, gint length);
  int gwp_plugin_document_get_position (gint docid);
  selection_range gwp_plugin_document_get_selection_range (gint docid);
  void gwp_plugin_document_set_selection_range (gint docid,
						selection_range sr);

  void gwp_plugin_program_register (plugin_info *info);
  gboolean gwp_plugin_program_quit ();

  extern GList *go_plugins;
  void gwp_setup_plugin_callbacks (plugin_callback_struct *callbacks);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GWP_PLUGIN_API_H__ */
