/*
 * Copyright � 1997 The Hungry Programmers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __wputils_included__
#define __wputils_included__

#include "wp_debug.h"

typedef int boolean;
/*typedef unsigned char byte;*/

char *tabber (int d);


typedef struct
{
  long int x;
  long int y;
} WpPoint;


typedef struct
{
  WpPoint a;
  WpPoint b;
} WPRectangle;


typedef struct
{
  unsigned long int x;
  unsigned long int y;
} WpDimension;

WpPoint newWpPoint (long int x, long int y);
char *wp_resize_maybe (char *data, int *alloced, int szof, int needed);
char *itoa (int i);
char *wp_unique_document_name ();

#endif /*__wputils_included__*/
